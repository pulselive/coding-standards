# Pulselive CSS Coding standards #

This document outlines...

## Objectives ##

* Provide consistency across FE projects
* Make developes lives as simple as possible when writing CSS

## Potential deliverables ##

* Coding Standards
* Component Library
* Stylelint rules/config file